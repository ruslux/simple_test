#! coding:utf-8


def create_collection_class(name):
    """
    Create class by name.
    >>> AccountClass = create_collection_class("author")
    >>> account_instance = AccountClass()
    >>> account_instance.number = "7910000000000"
    :param name: `string`
    :return: `type`
    """
    class Meta(object):
        def __init__(self):
            self._number = "0" * 13

        def get_number(self):
            return self._number

        def set_number(self, value):
            if isinstance(value, str):
                if len(value) is 13:
                    self._number = value
                else:
                    raise ValueError(
                        "Wrong string length, expected 13 characters, got {length}"
                        .format(length=len(value))
                    )

            else:
                raise TypeError("Number must be `str` type")

        number = property(get_number, set_number)

    class_name = name.lower().title()
    return type(
        class_name, (Meta,), dict(
            COLLECTION=class_name
        )
    )
