import unittest
from run import create_collection_class


class TestCreationMethod(unittest.TestCase):
    def test_class_returned(self):
        self.assertEqual(
            isinstance(create_collection_class("account"), type), True
        )

    def test_name_of_class_is_correct(self):
        self.assertEqual(
            create_collection_class("account").__name__, "Account"
        )

    def test_has_constant_COLLECTION(self):
        self.assertEqual(
            hasattr(create_collection_class("account"), "COLLECTION"), True
        )

    def test_name_of_collection_is_correct(self):
        self.assertEqual(
            create_collection_class("account").COLLECTION, "Account"
        )
        self.assertEqual(
            create_collection_class("account").COLLECTION, create_collection_class("account").__name__
        )


class TestUseCreatedClass(unittest.TestCase):
    def test_property_placed_on_instance(self):
        instance = create_collection_class("Account")()

        self.assertEqual(hasattr(instance, "number"), True)
        self.assertEqual(type(instance.number), str)

    def test_number_has_13_characters(self):
        instance = create_collection_class("Account")()
        self.assertEqual(len(instance.number), 13)
        with self.assertRaises(ValueError):
            instance.number = "a" * 14
            instance.number = ""
        with self.assertRaises(TypeError):
            instance.number = 13
